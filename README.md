#   Property Guru iOS Technical Interview 
##  About this Assignment (Take note those in bold) 

1. This assignment is a simulation of how we work at PropertyGuru where code must be clean and properly tested before merge. 
2. Kindly submit your assignment in Swift >=4.2. We are interested to know how comfortable you are working with Structs, Enums, Extensions, Protocol Oriented Programming in Swift. 
3. As we want to assess your coding strength, we want you not to use any 3rd party libraries. (eg. No Alamo Fire, No Realm… etc) 
4. Testability – You are expected to have Unit and UI Tests with high coverage. Submit code coverage report if possible. TDD is highly encouraged. Unit Test should have >80% coverage. 
5. Kindly apply SOLID, appropriate modeling design to the assignment. Hint: A big fat ViewController will not cut it for us. 
6. The assignment may look simple but we will be critical on your decisions and reasoning on why you code in a certain way. 
7. You may be required to change and resubmit codes after code reviews conducted by PropertyGuru. 
8. Code is important to us. Your code should help us determine whether you are a good fit to our team. 
9. We would like you to use UICollectionView instead of UITableView. 
10. Add documentation where necessary to aid us in this assessment. 

## My NewYorkTimes Clone App 

1. As a user on app first launch, 
- When I reach the home page 
- Then I should a list (or gallery of news headlines). Each news item on this page should show i) title 
ii) snippet 
iii) date 
iv) image

2 And I should see a search box 
2a. As a user on the home page, 
- When I tap on a news item 
- Then I should see the news details. 

2b. As a user on the news details page, (Good to have) 
- When I swipe left or right 
- Then I should see the corresponding news before or after the current news item based on the order on the home page. 

3. As a user on the home page, 
- When I focus on to the search bar, 
- And enter search terms 
- And press enter - Then I should see the corresponding news result lists based on the search terms 
4A. As a user at the home page, 
- When I tap and focus into the search box 
- Then I should see a list of my previous 10 search terms 
4B. As a user at the home page with previous search terms shown, 
- When I select a search term item 
- Then I should see the corresponding news result lists based on the selected search term 
5. As a user on the home page with news list shown, 
- When I scroll near the end of the list, 
- Then the next page should be loaded (ie “infinite scroll”) 
Example Api to use: 
https://api.nytimes.com/svc/search/v2/articlesearch.json?api-key=5763846de30d489aa867f0711e2b031c&q=singapore&page=0


## Code Coverage

link: https://bitbucket.org/DhanMoti/pg-tech-challenge-nyt-clone/src/master/Attachments/code-coverage-rpt.png
