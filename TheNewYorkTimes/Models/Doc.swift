//
//  Docs.swift
//  TheNewYorkTimes
//
//  Created by Dhan Moti on 30/4/19.
//  Copyright © 2019 DeviDhan. All rights reserved.
//

import Foundation

struct Doc: Codable {
    var webURL: String
    var snippet: String
    var pubDate: String
    var multimedia: [Multimedia]
    var headline: Headline
    
    enum CodingKeys: String, CodingKey {
        case webURL = "web_url"
        case snippet
        case pubDate = "pub_date"
        case multimedia
        case headline
    }
}
