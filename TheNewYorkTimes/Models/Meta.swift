//
//  Meta.swift
//  TheNewYorkTimes
//
//  Created by Dhan Moti on 30/4/19.
//  Copyright © 2019 DeviDhan. All rights reserved.
//

import Foundation

struct Meta: Codable {
    var hits: Int
    var offset: Int
    var time: Int
}
