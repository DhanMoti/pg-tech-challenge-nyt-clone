//
//  Response.swift
//  TheNewYorkTimes
//
//  Created by Dhan Moti on 30/4/19.
//  Copyright © 2019 DeviDhan. All rights reserved.
//

import Foundation
struct Response: Codable {
    var docs: [Doc]
    var meta: Meta
}

struct ArticleSearchResponse: Codable {
    var status: String
    var copyright: String
    var response: Response
}
