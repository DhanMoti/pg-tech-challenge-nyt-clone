//
//  Multimedia.swift
//  TheNewYorkTimes
//
//  Created by Dhan Moti on 30/4/19.
//  Copyright © 2019 DeviDhan. All rights reserved.
//

import Foundation


struct Multimedia: Codable {
    var url: String
    var type: String
    
    
}
