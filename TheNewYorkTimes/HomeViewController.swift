//
//  HomeViewController.swift
//  TheNewYorkTimes
//
//  Created by Dhan Moti on 29/4/19.
//  Copyright © 2019 DeviDhan. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet var collectionView: UICollectionView!
    
    
    let cellIdentifier = "docs-cell"
    let detailSegue = "detail-segue"
    let searchSegue = "search-segue"
    
    private var query = ""
    private var page = 1
    
    private var docs = [Doc]()
    private var selectedDoc: Doc?
    private var fetcher = ArticleFetcher()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        loadDocs()
    }
    

    // MARK: - Actions
    @IBAction func searchButtonTap(_ sender: Any) {
        self.performSegue(withIdentifier: searchSegue, sender: self)
    }
    
    func loadDocs(loadMore: Bool = false) {
        let baseURL = "\(Constants.API.baseURL)\(Constants.API.articleSearch)?api-key=\(Constants.API.APIKey)"
        let url = "\(baseURL)&q=\(query)&page=\(page)"
        
        fetcher.fetch(urlStr: url) { (result) in
            switch result {
                
            case .failure(let err):
                print(err.localizedDescription)
                
            case .success(let r):
                if loadMore { self.docs.append(contentsOf: r.response.docs) }
                else{  self.docs = r.response.docs }
                
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                    if !loadMore {
                        self.collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: UICollectionView.ScrollPosition.top, animated: true)
                    }
                }
            }
            
        }
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if let vc = segue.destination as? DetailViewController, let d = selectedDoc {
            vc.doc = d
        }
        else if let vc = segue.destination as? SearchViewController {
            vc.delegate = self
        }
    }
 

    
    
    
    
}

 // MARK: - UICollectionViewDelegate, UICollectionViewDataSource
extension HomeViewController : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return docs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as? DocPreviewCollectionViewCell {
            cell.configureCell(doc: docs[indexPath.row])
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedDoc = docs[indexPath.item]
        self.performSegue(withIdentifier: detailSegue, sender: self)
    }
    
    // TODO: prefech data
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.item >= docs.count - 2 {
            page = page + 1
            loadDocs(loadMore: true)
        }
    }
}


extension HomeViewController: SearchViewDelegate {
    func didSearchButtonClick(query: String) {
        self.query = query
        self.page = 0
        self.loadDocs()
    }
}
