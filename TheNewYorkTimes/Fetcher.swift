//
//  Fetcher.swift
//  TheNewYorkTimes
//
//  Created by Dhan Moti on 30/4/19.
//  Copyright © 2019 DeviDhan. All rights reserved.
//

import UIKit

public enum Result<Value> {
    case success(Value)
    case failure(Error)
}

class Fetcher<T: Codable> {
    func fetch(urlStr:String, onComplete: @escaping (Result<T>) -> Void) {
        let session = URLSession.shared
        let url = URL(string: urlStr)!
        
        let request = URLRequest(url: url)
        
        let task = session.dataTask(with: request) { (data, res, error) in
            guard let data = data else {
                onComplete(Result.failure(error!))
                return
            }
            
            let decoder = JSONDecoder()
            do {
                let decoded = try decoder.decode(T.self, from: data)
                onComplete(Result.success(decoded))
            }
            catch let error {
                print(error.localizedDescription)
                onComplete(Result.failure(error))
            }
           
        }
        
        task.resume()
    }
    
}

typealias ArticleFetcher = Fetcher<ArticleSearchResponse>


class DataFetcher: Fetcher<Data> {
    override func fetch(urlStr: String, onComplete: @escaping (Result<Data>) -> Void) {
        let session = URLSession.shared
        let url = URL(string: urlStr)!
        let request = URLRequest(url: url, cachePolicy: .returnCacheDataElseLoad, timeoutInterval: 60)
        
        
        let task = session.dataTask(with: request) { (data, res, error) in
            guard let data = data else {
                onComplete(Result.failure(error!))
                return
            }
            onComplete(Result.success(data))
        }
        
        task.resume()
    }
}
