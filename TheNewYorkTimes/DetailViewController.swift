//
//  DetailViewController.swift
//  TheNewYorkTimes
//
//  Created by Dhan Moti on 29/4/19.
//  Copyright © 2019 DeviDhan. All rights reserved.
//

import UIKit
import WebKit

class DetailViewController: UIViewController {

    @IBOutlet var webView: WKWebView!
    
    var doc: Doc?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if let d = doc, let url = URL(string: d.webURL) {
            webView.load(URLRequest(url: url))
        }
        
    }
    
 

}
