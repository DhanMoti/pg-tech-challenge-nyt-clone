//
//  DocPreviewCollectionViewCell.swift
//  TheNewYorkTimes
//
//  Created by Dhan Moti on 30/4/19.
//  Copyright © 2019 DeviDhan. All rights reserved.
//

import UIKit

class DocPreviewCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var snippetLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    
    var fetcher = DataFetcher()
    
  
    
    func configureCell(doc: Doc){
        titleLabel.text = doc.headline.main
        snippetLabel.text = doc.snippet
        dateLabel.text = doc.pubDate
        imageView.image = nil
        
        if doc.multimedia.count == 0 { return }
        let media = doc.multimedia[0]
        
        fetcher.fetch(urlStr: "\(Constants.API.domain)\(media.url)") { (result) in
            switch result {
                
            case .failure(let err):
                print(err.localizedDescription)
                
            case .success(let data):
                DispatchQueue.main.async {
                    self.imageView.image = UIImage(data: data)
                }
            }
            
           
        }
    }
        
    
    
}
