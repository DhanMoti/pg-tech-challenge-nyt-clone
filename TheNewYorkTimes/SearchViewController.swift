//
//  SearchViewController.swift
//  TheNewYorkTimes
//
//  Created by Dhan Moti on 30/4/19.
//  Copyright © 2019 DeviDhan. All rights reserved.
//

import UIKit

protocol SearchViewDelegate {
    func didSearchButtonClick(query: String)
}

class SearchViewController: UIViewController {
    var delegate: SearchViewDelegate?
    
    @IBOutlet var searchbar: UISearchBar!
    @IBOutlet var tableview: UITableView!
    
    var recents = [String]()
    var database = RecentSearchDatabase()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        recents = database.getList()
        tableview.reloadData()
        
    }
    
    
    func handleSearch(query: String){
        if let d = delegate {
            d.didSearchButtonClick(query: query)
            self.database.save(value: query)
            self.navigationController?.popViewController(animated: true)
        }
        
    }

}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Recents"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "search-cell"){
            cell.textLabel?.text = recents[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let q = recents[indexPath.row]
        handleSearch(query: q)
    }
}

extension SearchViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let q = searchBar.text {
            handleSearch(query: q)
        }
    }
}
