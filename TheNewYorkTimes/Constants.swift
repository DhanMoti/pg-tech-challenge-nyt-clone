//
//  Constants.swift
//  TheNewYorkTimes
//
//  Created by Dhan Moti on 1/5/19.
//  Copyright © 2019 DeviDhan. All rights reserved.
//

import UIKit

struct Constants {
    
    struct API {
        static let domain = "https://www.nytimes.com/"
        static let baseURL = "https://api.nytimes.com/svc/search/v2/"
        static let articleSearch = "articlesearch.json"
        static let APIKey = "Ewg6GvdFShXtGTdV0IyDvDiV73A61SN4"
        
    }
}

