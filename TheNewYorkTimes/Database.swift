//
//  Database.swift
//  TheNewYorkTimes
//
//  Created by Dhan Moti on 30/4/19.
//  Copyright © 2019 DeviDhan. All rights reserved.
//

import UIKit



protocol SaveDatabase {
    func save(value : String)
}

protocol ListDatabase {
    func getList() -> [String]
}

class RecentSearchDatabase: SaveDatabase, ListDatabase {
    
    func save(value: String) {
        var recents = getList()
        
        if recents.contains(value) { return }
        
        if recents.count >= 10 { recents.removeLast()}
        recents.insert(value, at: 0)
        UserDefaults.standard.set(recents, forKey: "recent-searches")

    }
    
    func getList() -> [String] {
        guard let recents = UserDefaults.standard.array(forKey: "recent-searches") as? [String]
            else {return []}
        return recents
    }
    
    
}
