//
//  TheNewYorkTimesTests.swift
//  TheNewYorkTimesTests
//
//  Created by Dhan Moti on 29/4/19.
//  Copyright © 2019 DeviDhan. All rights reserved.
//

import XCTest
@testable import TheNewYorkTimes

class TheNewYorkTimesTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testFetchAllArticles(){
        // Create an expectation object.
        let fetchExpectation = expectation(description: "page 1 is fetched.")
        let fetcher = ArticleFetcher()
        let url = "\(Constants.API.baseURL)\(Constants.API.articleSearch)?api-key=\(Constants.API.APIKey)&q=&page=0"
        
        fetcher.fetch(urlStr: url) { (result) in
            switch result {
            case .success(let r):
                XCTAssert(r.response.docs.count > 0)
                fetchExpectation.fulfill()
            case .failure(_):
                XCTFail("Could not fetch articles")
            }
        }
        
        waitForExpectations(timeout: 15, handler: nil)
    }
    
    func testFetchArticlesWithQuery(){
        // Create an expectation object.
        let fetchExpectation = expectation(description: "page 1 is fetched.")
        let fetcher = ArticleFetcher()
        let url = "\(Constants.API.baseURL)\(Constants.API.articleSearch)?api-key=\(Constants.API.APIKey)&q=Singapore&page=0"
        
        fetcher.fetch(urlStr: url) { (result) in
            switch result {
            case .success(let r):
                XCTAssert(r.status == "OK")
                fetchExpectation.fulfill()
            case .failure(_):
                XCTFail("Could not fetch articles")
            }
        }
        
        waitForExpectations(timeout: 15, handler: nil)
    }

    func testRecentSearch(){
        let queryToSave = "Search String 333"
        let saveExpectation = expectation(description: "save to user default")
        let database = RecentSearchDatabase()
        
        database.save(value: queryToSave)
        
        let recents = database.getList()
        
        XCTAssert(recents.contains(queryToSave))
        saveExpectation.fulfill()
        
        waitForExpectations(timeout: 5, handler: nil)
        
    }
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
