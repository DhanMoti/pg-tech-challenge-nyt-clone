//
//  TheNewYorkTimesUITests.swift
//  TheNewYorkTimesUITests
//
//  Created by Dhan Moti on 29/4/19.
//  Copyright © 2019 DeviDhan. All rights reserved.
//

import XCTest

class TheNewYorkTimesUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    func testLoadDetailView(){
        
        let app = XCUIApplication()
        let cell = app.collectionViews.children(matching: .any).element(boundBy: 0)
        expectation(for: NSPredicate(format: "exists == 1"), evaluatedWith: cell, handler: nil)
        waitForExpectations(timeout: 15, handler: nil)
        
        XCTAssertTrue(cell.exists)
    
        let titleLabel = cell.staticTexts.element(boundBy: 0).label
        cell.tap()
        
        let predicate = NSPredicate(format: "label CONTAINS %@", titleLabel)
        let sometextInWebView = app.webViews.staticTexts.element(matching: predicate)
        expectation(for: NSPredicate(format: "exists == 1"), evaluatedWith: sometextInWebView, handler: nil)
        waitForExpectations(timeout: 15, handler: nil)
        
        XCTAssertTrue(sometextInWebView.exists)
        
    }
    
    func testSearchDocs(){
        let query = "Singapore"
        
        let app = XCUIApplication()
        let searchBarButton = app.navigationBars["The New York Times"].buttons["Search"]
        searchBarButton.tap()
        
        let searchBar = app.otherElements.containing(.navigationBar, identifier:"Recent Search").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .searchField).element
        
        expectation(for: NSPredicate(format: "exists == 1"), evaluatedWith: searchBar, handler: nil)
        waitForExpectations(timeout: 5, handler: nil)
        
        XCTAssertTrue(searchBar.exists)
        searchBar.tap()
        searchBar.typeText(query)
        
        let searchButton = app/*@START_MENU_TOKEN@*/.buttons["Search"]/*[[".keyboards.buttons[\"Search\"]",".buttons[\"Search\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        XCTAssertTrue(searchButton.exists)
        searchButton.tap()
        
        let predicateQuery = NSPredicate(format: "label CONTAINS %@", query)
        let titleWithQuery =  app.collectionViews.staticTexts.element(matching: predicateQuery)
        expectation(for: NSPredicate(format: "exists == 1"), evaluatedWith: titleWithQuery, handler: nil)
        waitForExpectations(timeout: 15, handler: nil)
        
        
        XCTAssertTrue(titleWithQuery.exists)
        
        
        searchBarButton.tap()
        let recentCell = app.tables.staticTexts[query]
        XCTAssertTrue(recentCell.exists)
        recentCell.tap()
        app.collectionViews.staticTexts.element(matching: predicateQuery)
       
        
        
    }
    

    func testConfigurePreviewCell(){
        
        let app = XCUIApplication()
        let collectionViewsQuery = app.collectionViews
        let cell = collectionViewsQuery.cells.children(matching: .any).element(boundBy: 0)
        
        expectation(for: NSPredicate(format: "exists == 1"), evaluatedWith: cell, handler: nil)
        waitForExpectations(timeout: 15, handler: nil)
        
        XCTAssertTrue(cell.exists)
        
        let label = cell.staticTexts.element(boundBy: 0)
        print("what is this  print(label)")
        print(label)
        XCTAssertTrue(label.exists)
        
        cell.swipeUp()
        
        
     
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

  
}
